package com.simones.criminalintent;

import android.support.v4.app.Fragment;

/**
 * Created by simon on 6/03/15.
 */
public class CrimeListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new CrimeListFragment();
    }
}
