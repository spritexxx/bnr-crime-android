package com.simones.criminalintent;

import java.util.UUID;
import java.util.Date;

/**
 * Created by simon.
 */
public class Crime {
    // crime identifier
    private UUID id;
    // title for the crime
    private String title;
    private Date creationDate;
    private boolean solved;
    
    public Crime () {
        // generate unique id
        id = UUID.randomUUID();
        creationDate = new Date();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public UUID getId() {
        return id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isSolved() {
        return solved;
    }

    public void setSolved(boolean solved) {
        this.solved = solved;
    }
    
    @Override
    public String toString()
    {
        return title;
        
    }
}
