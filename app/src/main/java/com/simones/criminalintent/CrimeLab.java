package com.simones.criminalintent;

import android.content.Context;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by simon on 6/03/15.
 */
public class CrimeLab {
    private Context appContext;
    private static CrimeLab crimeLab;
    
    private ArrayList<Crime> crimeList;

    private CrimeLab(Context appContext) {
        this.appContext = appContext;
        this.crimeList = new ArrayList<Crime>();
        generateCrimes();
    }

    public static CrimeLab getInstance(Context c) {
        if(crimeLab == null)
            crimeLab = new CrimeLab(c.getApplicationContext());
        
        return crimeLab;
    }
    
    public ArrayList<Crime> getCrimeList()
    {
        return crimeList; 
    }

    /**
     * Find crime with given UUID in the list or null if not found. 
     * @param id
     * @return
     */
    public Crime getCrime(UUID id) {
        for(Crime c : crimeList) {
            if(c.getId().equals(id))
                return c;
        }
        
        return null;
    }

    // generate test crimes...
    private void generateCrimes()
    {
        for(int i = 0; i < 100; i++) {
            Crime c = new Crime();
            c.setTitle("Crime # "+i);
            c.setSolved(i %2 == 0);
            crimeList.add(c);
        }
        
    }
}
