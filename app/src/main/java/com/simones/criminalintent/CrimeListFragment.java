package com.simones.criminalintent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by simon on 6/03/15.
 */
public class CrimeListFragment extends ListFragment{
    private ArrayList<Crime> crimes;
    private static final String TAG = "CrimeListFragment";
    
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(R.string.crimes_title);
        crimes = CrimeLab.getInstance(getActivity()).getCrimeList();

        CrimeAdapter adapter = new CrimeAdapter(crimes);
        setListAdapter(adapter);
    }
    
    @Override
    public void onResume()
    {
        super.onResume();
        ((CrimeAdapter)getListAdapter()).notifyDataSetChanged();
        
    }
    
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) 
    {
        Crime c = ((CrimeAdapter)getListAdapter()).getItem(position);
        
        //start  CrimeActivity
        Intent i = new Intent(getActivity(), CrimeActivity.class);
        i.putExtra(CrimeFragment.EXTRA_CRIME_ID, c.getId());
        startActivity(i);
        
    }
    
    private class CrimeAdapter extends ArrayAdapter<Crime> 
    {
        public CrimeAdapter(ArrayList<Crime> crimes) 
        {
            super(getActivity(), 0, crimes); 
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            // if we weren't given one, create new 
            if(convertView == null)
                convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item_crime, null);
            
            // Configure the view for this crime
            Crime c = getItem(position);

            TextView titleTextView = (TextView)convertView.findViewById(R.id.crime_list_titleTextView);
            titleTextView.setText(c.getTitle());
            
            TextView dateTextView = (TextView)convertView.findViewById(R.id.crime_list_dateTextView);
            dateTextView.setText(c.getCreationDate().toString());

            CheckBox solvedCheckBox = (CheckBox)convertView.findViewById(R.id.crime_list_item_solvedCheckBox);
            solvedCheckBox.setChecked(c.isSolved());
            
            return convertView;
            
        }
    }
}
